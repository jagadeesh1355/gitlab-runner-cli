# Gitlab Runner CLI
This extremly small project helps you to manage your runners in gitlab. If you have 100 projects and suddenly have to change the runner, you would normally have to open each project, go to the runners page, eleminate the old runner and add the new one. This tool does that for you.

## How-to use locally
1. Define 3 environment variables: 
    * GITLAB_TOKEN: An access token that has API access
    * GITLAB_RUNNER_IDS: A comma-separated string of the runner IDs you want to enable, e.g. 3523343,4255343,2221212
    * GITLAB_GROUP_NAME: The gitlab group name or your private namespace
2. Do an `npm install`
3. Run with `node RunnerCLI`

## How-to use via gitlab
The project has a gitlab-ci.yml that contains a manual action called `execute`. You still have to define the environment variables from above as project secrets, but afterwards you can just hit the play button in the Gitlab pipeline!